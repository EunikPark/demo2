variable "vpc_cidr" {
    default = "192.168.10.0/24"
}

variable "subnet_cidr" {
    default = {
    lex_private_db_1 = "192.168.10.0/28"
    lex_private_db_2 = "192.168.10.16/28"
    lex_private_web_1 = "192.168.10.32/28"
    lex_private_web_2 = "192.168.10.48/28"
    lex_public_1 = "192.168.10.64/28"
    lex_public_2 = "192.168.10.80/28"
    }
}


variable "RDS_PASSWORD" {
    default = "1q2w3e4r"
}

variable "WEB_INSTANCE_AMI" { default = "ami-03b42693dc6a7dc35" }

variable "API_INSTANCE_AMI" { default = "ami-03b42693dc6a7dc35" }

variable "PATH_TO_PUBLIC_KEY" {
  default = "lex_key.pub"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "lex_key"
}
variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}