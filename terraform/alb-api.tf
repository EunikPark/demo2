#########################################################
#                   api alb                             #
#########################################################

resource "aws_alb" "lex_api_alb" {
    name                = "lex-api-alb"
    internal            = true
    load_balancer_type  = "application"
    security_groups = [aws_security_group.sgroup_api_alb.id]
    subnets = [
        aws_subnet.lex_private_web_1.id,
        aws_subnet.lex_private_web_2.id
    ]

    enable_cross_zone_load_balancing = true

    
    tags = { Name = "Lex api alb" }
}

# target

resource "aws_alb_target_group" "api_alb_target" {
    name        = "api-alb-target"
    port        = 80
    protocol    = "HTTP"
    vpc_id      =  aws_vpc.lex_main_vpc.id 
    tags = { Name = "api alb target group" }
    stickiness {
        enabled = false
        type = "lb_cookie"
    }

}

resource "aws_alb_target_group_attachment" "api_alb_target" {
    count = aws_autoscaling_group.api_autoscaling.min_size
    target_group_arn = aws_alb_target_group.api_alb_target.arn
    target_id = element(data.aws_instances.test_api.ids, count.index)
    depends_on = [data.aws_instances.test_api]
    port = 80

}

data "aws_instances" "test_api" {
    instance_tags = {
        "aws:autoscaling:groupName" = aws_autoscaling_group.api_autoscaling.name
    }

    depends_on = [aws_launch_configuration.api_launchconfig, aws_autoscaling_group.api_autoscaling]
}


# listener


resource "aws_alb_listener" "api_alb_listener" {
    load_balancer_arn = aws_alb.lex_api_alb.arn
    port              = 80
    protocol          = "HTTP"
    # ssl_policy        = "ELBSecurityPolicy-2016-08"
    # certificate_arn   = "${data.aws_acm_certificate.example_dot_com.arn}"

    default_action {
        target_group_arn = aws_alb_target_group.api_alb_target.arn
        type             = "forward"
    }
}



# api alb's security group
resource "aws_security_group" "sgroup_api_alb" {
    vpc_id = aws_vpc.lex_main_vpc.id
    name = "sgroup-api-alb"
    description = "Lex Api-Alb Security Group"

    ingress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     =  ["0.0.0.0/0"]

    }
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     =  ["0.0.0.0/0"]
    }
    tags = { Name = "sgroup_api_alb" }
}

