#########################################################
#                   My SQL DB                           #
#########################################################


resource "aws_db_subnet_group" "mysql_subnet" {
    name = "lex-mysql-subnet"
    description = "RDS subnet group"
    subnet_ids = [ aws_subnet.lex_private_db_1.id, aws_subnet.lex_private_db_2.id ]
}

resource "aws_db_parameter_group" "mysql-parameters" {
    name = "mysqldb-parameters"
    family = "mysql5.7"
    description = "mysqlDB parameter group"
}


resource "aws_db_instance" "mysql_db" {
    allocated_storage    = 100
    engine               = "mysql"
    engine_version       = "5.7"
    instance_class       = "db.t2.micro"
    identifier           = "lexmysqldb"
    name                 = "lexmysqldb"
    username             = "lex"
    password             = var.RDS_PASSWORD
    parameter_group_name = aws_db_parameter_group.mysql-parameters.name
    multi_az = true
    skip_final_snapshot  = true
    vpc_security_group_ids = [aws_security_group.allow-mysqldb.id]
    db_subnet_group_name = aws_db_subnet_group.mysql_subnet.id

}


resource "aws_security_group" "allow-mysqldb" {
    vpc_id = aws_vpc.lex_main_vpc.id
    name = "allow-mysqldb"
    description = "allow-mysqldb"
    ingress {
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        security_groups = [aws_security_group.sgroup_api_instance.id]              # allowing access from our api instance
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        self = true
    }
    tags = {
        Name = "allow-mysqldb"
    }
}                                       