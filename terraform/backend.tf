terraform {
  backend "s3" {
      bucket = "lex-build-landing-aws-jenkins-terraform"
      key = "demo2.terraform.ftstate"
      region = "ap-northeast-2"
  }
}