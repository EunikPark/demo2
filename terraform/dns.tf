resource  "aws_acm_certificate" "lexbuild_dot_com"   { 
    domain_name   = "*.lex-build.com"
    validation_method  = "DNS"
    # provider          = aws.east
}

resource "aws_route53_zone" "lex-build" {
    name = "lex-build.com"
    # private_zone = false
}

resource "aws_route53_zone" "lexdemo" {
    name = "lexdemo.tk"
    # private_zone = false
}
resource  "aws_acm_certificate" "lexdemo"   { 
    domain_name   = "*.lexdemo.tk"
    validation_method  = "DNS"
    # provider          = aws.east
}

resource "aws_route53_record" "lexdemo" {
    for_each = {
        for dvo in aws_acm_certificate.lexdemo.domain_validation_options : dvo.domain_name => {
        name   = dvo.resource_record_name
        record = dvo.resource_record_value
        type   = dvo.resource_record_type
        }
    }

    allow_overwrite = true
    name            = each.value.name
    records         = [each.value.record]
    ttl             = 60
    type            = each.value.type
    zone_id         = aws_route53_zone.lexdemo.zone_id

    depends_on = [aws_acm_certificate.lexdemo]
}

resource "aws_route53_record" "lexbuild_dot_com" {
    for_each = {
        for dvo in aws_acm_certificate.lexbuild_dot_com.domain_validation_options : dvo.domain_name => {
        name   = dvo.resource_record_name
        record = dvo.resource_record_value
        type   = dvo.resource_record_type
        }
    }

    allow_overwrite = true
    name            = each.value.name
    records         = [each.value.record]
    ttl             = 60
    type            = each.value.type
    zone_id         = aws_route53_zone.lex-build.zone_id

    depends_on = [aws_acm_certificate.lexbuild_dot_com]
}

resource "aws_route53_record" "route53_record" {
    # count   = var.environment == "prd" ? 1 : 0
    zone_id = aws_route53_zone.lex-build.id
    name    = "www"
    type    = "A"
    alias {
        name                   = aws_alb.lex_web_alb.dns_name
        zone_id                = aws_alb.lex_web_alb.zone_id
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "lexdemo_route53_record" {
    # count   = var.environment == "prd" ? 1 : 0
    zone_id = aws_route53_zone.lexdemo.id
    name    = "route53-record-public"
    type    = "A"
    alias {
        name                   = aws_alb.lex_web_alb.dns_name
        zone_id                = aws_alb.lex_web_alb.zone_id
        evaluate_target_health = false
    }
}



resource "aws_acm_certificate_validation" "example" {
    certificate_arn         = aws_acm_certificate.lexbuild_dot_com.arn
    # validation_record_fqdns = [for record in aws_route53_record.example : record.fqdn]
}




# resource "aws_route53_record" "dayonedevops_com_mx" {
#     zone_id = aws_route53_zone.dayonedevops_com.zone_id
#     name    = "dayonedevops.com"
#     type    = "MX"
#     ttl     = "3600"
#     records = [

#     ]
# }