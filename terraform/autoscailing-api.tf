#########################################################
#                   api autoscail                       #
#########################################################

resource "aws_launch_configuration" "api_launchconfig" {
    name_prefix          = "api-launchconfig-"
    image_id             = var.API_INSTANCE_AMI
    instance_type        = "t2.micro"
    security_groups      = [aws_security_group.sgroup_api_instance.id]

    # user_data = "${data.template_file.api-shell-script.rendered}"

    iam_instance_profile = aws_iam_instance_profile.CloudWatchAgentServerRole-instanceprofile.name

    connection {
        user = var.INSTANCE_USERNAME
        private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
    }

    lifecycle {
        create_before_destroy = true
    }

}



resource "aws_autoscaling_group" "api_autoscaling" {
    name = "${aws_launch_configuration.api_launchconfig.name}-lex-asg"

    vpc_zone_identifier  = [
        aws_subnet.lex_private_web_1.id,
        aws_subnet.lex_private_web_2.id
        ]
    launch_configuration = "${aws_launch_configuration.api_launchconfig.name}"
    min_size             = 2
    max_size             = 4
    health_check_grace_period = 300
    health_check_type = "ELB"
    # load_balancers = [aws_alb.lex_api_alb.name]
    force_delete = true

    lifecycle {
        create_before_destroy = true
    }

    tag {
        key = "Name"
        value = "api ec2 instance"
        propagate_at_launch = true
    }
}


# api autoscailing security goup
resource "aws_security_group" "sgroup_api_instance" {
    vpc_id = aws_vpc.lex_main_vpc.id
    name = "sgroup-api-instance"
    description = "Lex Api-instance Security Group"

    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "TCP"
        cidr_blocks     =  ["0.0.0.0/0"]

    }
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     =  ["0.0.0.0/0"]
    }
    tags = { Name = "sgroup_api_instance" }
}
