#aws provider
provider "aws" {
    region = "ap-northeast-2"
    # version = ""
    shared_credentials_file = "/Users/gkohli/.aws/credentials"
  
}

# provider "aws" {
#   alias  = "east"
#   region = "us-east-1"
#   version = "2.70.0"
# }

# resource "aws_vpc" "lex_main_vpc" {
#     cidr_block           = "192.168.10.0/24"
#     enable_dns_support   = true
#     enable_dns_hostnames = true

#     tags = { Name = "lex_main_vpc" }
# }