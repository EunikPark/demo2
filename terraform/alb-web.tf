#########################################################
#                   web alb                             #
#########################################################

resource "aws_alb" "lex_web_alb" {
    name                = "lex-web-alb"
    internal            = false
    load_balancer_type  = "application"
    security_groups = [aws_security_group.sgroup_web_alb.id]
    subnets = [
        aws_subnet.lex_public_1.id,
        aws_subnet.lex_public_2.id
    ]

    enable_cross_zone_load_balancing = true

    
    tags = { Name = "Lex web alb" }
}

# target

resource "aws_alb_target_group" "web_alb_target" {
    name        = "lex-web-alb-target"
    port        = 80
    protocol    = "HTTP"
    vpc_id      =  aws_vpc.lex_main_vpc.id 
    tags = { Name = "web alb target group" }

    lifecycle {
      create_before_destroy = true
    }

}

resource "aws_alb_target_group_attachment" "web_alb_target" {
    count = aws_autoscaling_group.web_autoscaling.min_size

    target_group_arn = aws_alb_target_group.web_alb_target.arn
    target_id = element(data.aws_instances.test_web.ids, count.index)
    depends_on = [data.aws_instances.test_web]
    port = 80

}


data "aws_instances" "test_web" {
    instance_tags = {
        "aws:autoscaling:groupName" = aws_autoscaling_group.web_autoscaling.name
    }

    depends_on = [aws_launch_configuration.web_launchconfig]
}

# listener

resource "aws_alb_listener" "web_alb_listner" {
    load_balancer_arn = aws_alb.lex_web_alb.arn
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-2016-08"
    certificate_arn = aws_acm_certificate_validation.example.certificate_arn

    default_action {
        target_group_arn = aws_alb_target_group.web_alb_target.arn
        type             = "forward"
    }
}



# web alb's security group
resource "aws_security_group" "sgroup_web_alb" {
    vpc_id = aws_vpc.lex_main_vpc.id
    name = "security group-web-alb"
    description = "Lex Web-Alb Security Group"

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "TCP"
        cidr_blocks     =  ["0.0.0.0/0"]

    }
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     =  ["0.0.0.0/0"]
    }
    tags = { Name = "sgroup_web_alb" }
}

