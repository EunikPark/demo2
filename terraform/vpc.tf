#########################################################
#                       VPC                             #
#########################################################
resource "aws_vpc" "lex_main_vpc" {
    cidr_block           = var.vpc_cidr
    enable_dns_support   = true
    enable_dns_hostnames = true

    tags = { 
      Name = "lex_main_vpc" 
      }
}

#########################################################
#                       SUBNETS                         #
#########################################################
resource "aws_subnet" "lex_private_db_1" {
  vpc_id = aws_vpc.lex_main_vpc.id
  cidr_block = var.subnet_cidr["lex_private_db_1"]
  map_public_ip_on_launch = "false"
  availability_zone = "ap-northeast-2a"

  tags = { Name = "lex_private_db_1" }
}
resource "aws_subnet" "lex_private_db_2" {
  vpc_id = aws_vpc.lex_main_vpc.id
  cidr_block = var.subnet_cidr["lex_private_db_2"]
  map_public_ip_on_launch = "false"
  availability_zone = "ap-northeast-2c"

  tags = { Name = "lex_private_db_2" }
}

resource "aws_subnet" "lex_private_web_1" {
  vpc_id = aws_vpc.lex_main_vpc.id
  cidr_block = var.subnet_cidr["lex_private_web_1"]
  map_public_ip_on_launch = "false"
  availability_zone = "ap-northeast-2a"

  tags = { Name = "lex_private_web_1" }
}
resource "aws_subnet" "lex_private_web_2" {
  vpc_id = aws_vpc.lex_main_vpc.id
  cidr_block = var.subnet_cidr["lex_private_web_2"]
  map_public_ip_on_launch = "false"
  availability_zone = "ap-northeast-2c"

  tags = { Name = "lex_private_web_2" }
}

resource "aws_subnet" "lex_public_1" {
  vpc_id = aws_vpc.lex_main_vpc.id
  cidr_block = var.subnet_cidr["lex_public_1"]
  map_public_ip_on_launch = "true"
  availability_zone = "ap-northeast-2a"

  tags = { Name = "lex_public_1" }
}
resource "aws_subnet" "lex_public_2" {
  vpc_id = aws_vpc.lex_main_vpc.id
  cidr_block = var.subnet_cidr["lex_public_2"]
  map_public_ip_on_launch = "true"
  availability_zone = "ap-northeast-2c"

  tags = { Name = "lex_public_2" }
}


#########################################################
#                  INTERNET GATEWAY                     #
#########################################################
resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.lex_main_vpc.id
  tags = {
    Name = "lex_main_vpc Internet Gateway"
  }
}


# NAT Gateway

resource "aws_eip" "lex_natgw_eip" {
  vpc = true
  tags = {
    Name= "lex eip"
  }
}
resource "aws_nat_gateway" "lex_nat_gateway" {
  allocation_id = aws_eip.lex_natgw_eip.id
  subnet_id     = aws_subnet.lex_public_1.id
  tags = {
    Name = "lex_nat_gateway"
    
  }
  depends_on = [aws_internet_gateway.main_gw]
}

#########################################################
#                  ROUTE TABLE                          #
#########################################################
resource "aws_route_table" "lex_private_route_table" {
  vpc_id = aws_vpc.lex_main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.lex_nat_gateway.id
  }

  tags = { Name = "lex_private_route_table" }
}

resource "aws_default_route_table" "lex_public_route_table" {

  default_route_table_id = aws_vpc.lex_main_vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = { Name = "lex_public_route_table" }
}

# resource "aws_route" "private_route_to_ngw" {
#   route_table_id         = aws_route_table.lex_private_route_table.id
#   destination_cidr_block = "0.0.0.0/0"
#   nat_gateway_id         = aws_nat_gateway.lex_nat_gateway.id
# }




###########################
# route table association #
###########################

# private subnet 
resource "aws_route_table_association" "lex_private_db_1" {
  subnet_id = aws_subnet.lex_private_db_1.id
  route_table_id = aws_route_table.lex_private_route_table.id
}
resource "aws_route_table_association" "lex_private_db_2" {
  subnet_id = aws_subnet.lex_private_db_2.id
  route_table_id = aws_route_table.lex_private_route_table.id
}
resource "aws_route_table_association" "lex_private_web_1" {
  subnet_id = aws_subnet.lex_private_web_1.id
  route_table_id = aws_route_table.lex_private_route_table.id
}
resource "aws_route_table_association" "lex_private_web_2" {
  subnet_id = aws_subnet.lex_private_web_2.id
  route_table_id = aws_route_table.lex_private_route_table.id
}

# public subnet
resource "aws_route_table_association" "lex_public_1" {
  subnet_id = aws_subnet.lex_public_1.id
  route_table_id = aws_vpc.lex_main_vpc.default_route_table_id
}
resource "aws_route_table_association" "lex_public_2" {
  subnet_id = aws_subnet.lex_public_2.id
  route_table_id = aws_vpc.lex_main_vpc.default_route_table_id
}



