#########################################################
#                   web autoscail                       #
#########################################################

resource "aws_launch_configuration" "web_launchconfig" {
    name_prefix          = "lex-web-launchconfig-"
    image_id             = var.WEB_INSTANCE_AMI
    instance_type        = "t2.micro"
    security_groups      = [aws_security_group.sgroup_web_instance.id]
    user_data = file("${path.module}/user_data/apache_install.sh")
    # user_data = "${data.template_file.web-shell-script.rendered}"
    associate_public_ip_address = true
    iam_instance_profile = aws_iam_instance_profile.CloudWatchAgentServerRole-instanceprofile.name

    connection {
        user = var.INSTANCE_USERNAME
        private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
    }

    lifecycle {
        create_before_destroy = true
    }

}

# resource "aws_launch_configuration" "web_launchconfig2" {
#     name = "lex web_launch"
#     image_id             = var.WEB_INSTANCE_AMI
#     instance_type        = "t2.micro"
# }

resource "aws_autoscaling_group" "web_autoscaling" {
    name = "lex-${aws_launch_configuration.web_launchconfig.name}-asg"

    vpc_zone_identifier  = [
        aws_subnet.lex_private_web_1.id,
        aws_subnet.lex_private_web_2.id
        ]
    launch_configuration = "${aws_launch_configuration.web_launchconfig.name}"
    min_size             = 2
    max_size             = 4
    health_check_grace_period = 300
    health_check_type = "ELB"
    # load_balancers = [aws_alb.lex_web_alb.name]
    force_delete = true

    lifecycle {
        create_before_destroy = true
    }

    tag {
        key = "Name"
        value = "web ec2 instance"
        propagate_at_launch = true
    }
}

# web autoscailing security goup
resource "aws_security_group" "sgroup_web_instance" {
    vpc_id = aws_vpc.lex_main_vpc.id
    name = "security group-web-instance"
    description = "Lex Wep-instance Security Group"

    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "TCP"
        cidr_blocks     =  ["0.0.0.0/0"]

    }

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "TCP"
        cidr_blocks     =  ["0.0.0.0/0"]

    }


    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     =  ["0.0.0.0/0"]
    }
    tags = { Name = "sgroup_web_instance" }
}
